<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');
    $sheep = new Animal("shaun");

echo "Name : " .$sheep->name ."<br>"; // "shaun"
echo "Legs : " .$sheep->legs ."<br>"; // 4
echo "cold blood : " .$sheep->coldblood ."<br> <br>"; // "no"

$kodok = new Frog("buduk");

echo "Name : " .$kodok->name ."<br>";
echo "Legs : " .$kodok->legs ."<br>";
echo "cold blood : " .$kodok->coldblood ."<br>";
echo $kodok->jump() ."<br> <br>"; // "hop hop"

$sungokong = new Ape("kera sakti");

echo "Name : " .$sungokong->name ."<br>";
echo "Legs : " .$sungokong->legs ."<br>";
echo "cold blood : " .$sungokong->coldblood ."<br>";
echo $sungokong->yell() ."<br> <br>" // "Auooo"


?>